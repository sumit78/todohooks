import React,{useState} from 'react';

const friendList =[
  {id:1,name:'Phoebe'},
  {id:2,name:'Rachel'},
  {id:3,name:'Ross'},
];

function Customhooks(){
  const [recipientID,setRecipientID] = useState(1);
  const isRecipientOnline = useFriendStatus(recipientID);

  function useFriendStatus(props){
  const isOnline = useFriendStatus(props.friendList.id)
  if(isOnline === null){
    return 'Loading...';
  }
  return isOnline ? 'online' : 'offline';
  }

  return(
    <>
    <div color = {isRecipientOnline ? 'green':'red'}></div>
    <select
      value = {recipientID}
      onChange = {e => setRecipientID(Number(e.target.value))}
      >
        {friendList.map(friend => (
          <option key = {friend.id} value={friend.id}>
            {friend.name}
          </option>
        ))}
      </select>
    </>
  )
}

export default Customhooks;