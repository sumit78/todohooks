import React,{useState} from 'react';
import './Mystyle.css'


function List(){
  const [leftList,setleftList] = useState([]);
  const [rightList,setRightList] = useState([]);

  const handleClick = () => {
    let inputValue = document.getElementById('inputItem').value;
    setleftList([
      ...leftList,
      {
        id:leftList.length,
        value:inputValue
      }
    ]);
  };

  const handlePush = (item,index) => {
    const newleftList = [...leftList ];
    newleftList.splice(index,1);
    setleftList(newleftList);
    console.log(leftList);

    setRightList([
      ...rightList,
      {
        id:rightList.length,
        value:item.value
      }
    ]);
  };

  return(
    <div>
    <div className="center">
    <input type="text" id="inputItem"/>
    <button className="button button2" type="button" onClick = {handleClick} >AddItem</button>
    </div>
    <div className="flex-container">
    <ul>
     {leftList.map((item,index)=>{
      return <li key={item.id} onClick={() => handlePush(item,index)}>{item.value}</li>
     })}
    </ul>
      <ul>
        {rightList.map((item1) => {
          return <li key={item1.id}>{item1.value}</li>
        })}
      </ul>
      </div>
  
    </div>
  )
}

export default List;
