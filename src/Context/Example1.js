import React from 'react';

const ThemeContext = React.createContext('dark');
class Example1 extends React.Component{
render(){
  return (
    <ThemeContext.Provider value="light">
        <Toolbar />
      </ThemeContext.Provider>
  );
}
}

function Toolbar(props){
  return(
    <div>
      <ThemedButton/>
    </div>
  );
}

class ThemedButton extends React.Component{
  static contextType = ThemeContext;
  render(){
    return <button theme = {this.context}>Add button</button>
  }
}

export default Example1;