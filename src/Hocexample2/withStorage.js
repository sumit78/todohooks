import React from 'react'


const withStorage = (WrappedComponent) => {
  class HOC extends React.Component{
    state = {
      localStorageAvailable:false,
    };
     
    componentDidMount(){
      this.checkLocalStorageExist();
    }; 

    checkLocalStorageExist(){
      const testkey = 'text';

      try{
       localStorage.setItem(testkey,testkey);
       localStorage.removeItem(testkey);
       this.setState({localStorage:true});
      }catch(e){
        this.setState({localStorage:true});
      }
    };

    load = (key) => {
      if(this.localStorageAvailable){
        localStorage.setItem(key);
      }
      return null;
    };

    save = (key,data) => {
      if(this.localStorageAvailable){
        localStorage.setItem(key,data);
      }
    };

    remove = (key) => {
      if(this.localStorageAvailable){
        localStorage.removeItem(key);
      }
    };

     render() {
      return(
        <WrappedComponent 
        load = {this.load}
        save = {this.save}
        remove = {this.remove}
        {...this.props}
        />
      )
    }
  }
  return HOC;
}

export default withStorage;