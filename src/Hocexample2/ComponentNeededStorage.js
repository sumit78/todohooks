import React from 'react';
import withStorage from './withStorage';

class ComponentNeededStorage extends React.Component{
  
  state={
    username:'',
    favorite:'',
  }

  componentDidMount(){
    const username = this.props.loads('username');
    const favorite = this.props.loads('favorite');

    if(!username || !favorite){

      this.props.reallyLongApiCall()
        .then((user) => {
          this.props.save('username',user.username) || '';
          this.props.save('favorite',user.favorite) || '';
          this.setState({
            username:user.username,
            favorite:user.favorite,
          });    
        });
    } else{
      this.setState({username,favorite});
      }
  }
    render(){
      const {username,favorite} = this.state;

        if(!username || !favorite){
          return <div> Loading..</div>
        }

      return(
        <div>
          My username is {username}, and i love watch {favorite}
        </div>
      )
    }
}
export default ComponentNeededStorage;
