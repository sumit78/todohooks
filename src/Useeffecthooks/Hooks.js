import React,{useEffect,useState} from 'react';


function Hooks(){
  const [count,setCount] = useState(0);


  useEffect(()=>{
   const id = setInterval(()=>{
     setCount(count + 1);
   },1000);
   return()=> clearInterval(id);
  },[count]);

  return(
    <>
    <p> you clicked {count} times</p>

    </>
  )
}

export default Hooks;