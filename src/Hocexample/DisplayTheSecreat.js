import React from 'react';
import withSecretToLife  from './WithSecretToLife';

const DisplayTheSecreat = props => (
  <div>
    the secreat to life is {props.secretToLife}
  </div> 
)

const wrappedComponent = withSecretToLife(DisplayTheSecreat);

export default wrappedComponent;
