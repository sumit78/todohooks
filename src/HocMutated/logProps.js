

function logProps(InputComponent){
  InputComponent.prototype.componentWillReceiveProps = function(nextProps){
    console.log('Current props:',this.props);
    console.log('next props:',nextProps);
  };
  return InputComponent;
} 
//const EnhancedComponent =logProps(InputComponent);

//export default EnhancedComponent;