import React from 'react';

function logProps (WrappedComponent){
  return class extends React.Component{
    componentWillReceiveProps(nextProps){
      console.log('Current props: ', this.props);
      console.log('Next props: ', nextProps);
    }
    render(){
      return <WrappedComponent  {...this.props}></WrappedComponent>
    }
  }
}
export default logProps;